package com.example.session05

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.session05.adapter.ContactoAdapter
import com.example.session05.model.Contacto
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var ListaContacto:MutableList<Contacto> = mutableListOf<Contacto>()
    lateinit var  adaptador:ContactoAdapter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        CargarDatos()
        ConfigurarAdaptador()





    }


    fun ConfigurarAdaptador(){

        adaptador= ContactoAdapter(ListaContacto)
        recyclerviewContacto.adapter=adaptador
        recyclerviewContacto.layoutManager=LinearLayoutManager(this)
        adaptador.onItemClick={

            val i=Intent(Intent.ACTION_CALL)
            i.data= Uri.parse("tel:${it.telefono}")
            startActivity(i)
        }







    }



    fun CargarDatos(){

        ListaContacto.add(
            Contacto(
                "Jose Luis",
                "Analista",
                "1123@gmail.com",
                "994207896"
            )
        )
        ListaContacto.add(
            Contacto(
                "Jorge Camilo",
                "Administrador ",
                "jorge@gmail.com",
                "994207895"
            )
        )
        ListaContacto.add(
            Contacto(
                "Rocio Miranda",
                "Analista",
                "Roci@gmail.com",
                "994207894"
            )
        )
        ListaContacto.add(
            Contacto(
                "Lucas Garcia",
                "Contador",
                "Lucas@hotmail.com",
                "994207893"
            )
        )
        ListaContacto.add(
            Contacto(
                "Eleonor Casillas",
                "Administrador",
                "Eleo@yahoo.com",
                "994207892"
            )
        )
        ListaContacto.add(
            Contacto(
                "Samira",
                "Soporte",
                "Sami@gmail.com",
                "994207891"
            )
        )


    }



    fun CargarAdaptador(){


    }

}
