package com.example.session05.adapter

import android.text.Layout
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.session05.R
import com.example.session05.model.Contacto
import kotlinx.android.synthetic.main.card_contactos.view.*



class ContactoAdapter (var contacts:MutableList<Contacto>):RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>(){

    lateinit var onItemClick:(item:Contacto)->Unit


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {

        val view:View=LayoutInflater.from(parent.context).inflate(R.layout.card_contactos,parent,false)

            return ContactoAdapterViewHolder(view)
     }

    override fun getItemCount(): Int {
        return contacts.size
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {

        val contact=contacts[position]
        holder.tvNombre.text=contact.nombres
        holder.tvCargo.text=contact.cargo
        holder.tvCorreo.text=contact.correo
        holder.btnLetra.text=contact.nombres.substring(0,1)


        holder.imgContacto.setOnClickListener{
            onItemClick(contact)

        }





    }


    class ContactoAdapterViewHolder (itemView:View):RecyclerView.ViewHolder(itemView){
        val tvNombre:TextView=itemView.findViewById(R.id.tvNombre)
        val tvCargo:TextView=itemView.findViewById(R.id.tvCargo)
        val tvCorreo:TextView=itemView.findViewById(R.id.tvCorreo)
        val btnLetra:Button=itemView.findViewById(R.id.btnLetra)
        val imgContacto:ImageView=itemView.findViewById(R.id.ivTelefono)

    }



}